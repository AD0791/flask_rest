# Rest API with flask

this tutorial is focusing mostly on building the API, so I’m using the mocked data. In most cases, while you are making API, it would be connected to the database.

We will go through the following points during the development:

1. Installing flask and flask_restful
2. Create and initialize the file
3. Mocked data
4. Create StudentsList class and route
5. Create get() and post() methods for StudentsList()
6. Define Student class and route
7. Create get(), update() and delete() methods
8. Test the endpoints
9. To make it easier and more convenient, I prepared a video version of this tutorial for those who prefer learning from the movies.
